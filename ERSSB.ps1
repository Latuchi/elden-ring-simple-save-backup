Get-ChildItem $env:APPDATA/EldenRing/ -directory | foreach {
$get_time = $_.LastWriteTime.ToShortTimeString()
$time = Get-Date $get_time -Format "HHmm"
$get_date = $_.LastWriteTime.ToShortDateString()
$date = Get-Date $get_date -UFormat "%d-%m-%Y"
$curr_time = [DateTimeOffset]::Now.ToUnixTimeSeconds()
$curr_time = $curr_time.ToString()
$new_folder = $curr_time + "_" + $time + "_" + $date
$destination = "$env:HOMEPATH\EldenRing_Backup\$new_folder"

if (test-path $destination){ 
    copy-item -Path $env:APPDATA\EldenRing\ -destination $destination -Recurse
    } else {
    new-item -ItemType directory -Path $destination
    copy-item -Path $env:APPDATA\EldenRing\ -destination $destination -Recurse
    }
}

Invoke-Item $destination\EldenRing
