# Elden Ring Simple Save Backup

ERSSB allows users to easily create a backup of their Elden Ring save files with a simple Powershell script. It will copy the save files from their current location to a new folder in users home path. This makes it easy to keep multiple versions of the save files, in case of any corruption or accidental deletion.


## DISCLAIMER

This script is provided "as is" without any warranties or guarantees. The author of this script shall not be held responsible for any direct, indirect, incidental, consequential, or other damages resulting from the use of this script. The user of this script assumes all risk and responsibility for any and all consequences arising from its use.

## Getting started

In order to run the script you need to make sure your Powershell execution policy for scope *`CurrentUser`* **or** *`LocalMachine`* is either Unrestricted or Bypass.

To check the current execution policies, open Powershell and type in `Get-ExecutionPolicy -List`. This will give you a list of the current execution policies.

To change execution policy for *`CurrentUser`*, open Powershell normally and type in `Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted`.

However if you want to change the execution policy for *`LocalMachine`*, you will need to open Powershell as an administrator and Once Powershell is open as an admin, type in `Set-ExecutionPolicy -Scope LocalMachine -ExecutionPolicy Unrestricted`. This will set the execution policy for LocalMachine to Unrestricted.

After the execution policy is set correctly, right-clicking on the script file and selecting `Run with PowerShell` will execute the script. Upon completion, the backup folder will open and the user can confirm that their files are backed up.

[More about Powershell execution policies](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies#powershell-execution-policies "More about Powershell execution policies")
